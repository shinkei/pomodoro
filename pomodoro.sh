#!/bin/bash
#
# Minimalistic_Pomodoro_Timer
#
# Based on the SU answer found here: https://superuser.com/questions/224265/pomodoro-timer-for-linux/669811#669811
#
# Tested in Ubuntu 16.04 and Arch
pomodoroMessage () {
  notify-send "Time to Work" "Focus" -u normal -a 'Pomodoro' -i $HOME/Documents/icon.png
  paplay /usr/share/sounds/freedesktop/stereo/window-attention.oga
}

shortbreakMessage () {
  notify-send "Short Break Time" -u critical -a 'Pomodoro' -i $HOME/Documents/icon.png
  paplay /usr/share/sounds/freedesktop/stereo/complete.oga
  # uncomment line below to lock your screen during the short break (needs to install xtrlock for your distribution)
  # sleep 5 && xtrlock -b & sleep 250 && pkill xtrlock
}

longbreakMessage () {
  notify-send "Long Break Time" "Take a Rest" -u critical -a 'Pomodoro' -i $HOME/Documents/icon.png
  paplay /usr/share/sounds/freedesktop/stereo/complete.oga
}

pomodoroWithShortBreak () {
  now=`date +"%H:%M"`
  printf "\033[A"
  pomodoroMessage
  echo "Pomodoro number: $1 started at: $now"
  countdown "00:25:00"
  shortbreakMessage
  printf "\033[A"
  countdown "00:05:00"
}

pomodoroWithLongBreak () {
  now=`date +"%H:%M"`
  printf "\033[A"
  pomodoroMessage
  echo -e "Pomodoro number: $1 started at: $now"
  countdown "00:25:00"
  longbreaktime
  printf "\033[A"
  countdown "00:15:00"
}

countdown(){
  IFS=:
  set -- $*
  secs=$(( ${1#0} * 3600 + ${2#0} * 60 + ${3#0} ))
  while [ $secs -gt 0 ]
  do
    sleep 1 &
    printf "\r%02d:%02d:%02d" $((secs/3600)) $(((secs/60)%60)) $((secs%60))
    secs=$(( $secs - 1 ))
    wait
  done
  echo
}

case "$1" in
  'start')
    echo "Starting Pomodoro"
    echo " "
    counter=0
    while true; do
      counter=$((counter+1))
      pomodoroWithShortBreak $counter
      counter=$((counter+1))
      pomodoroWithShortBreak $counter
      counter=$((counter+1))
      pomodoroWithShortBreak $counter
      counter=$((counter+1))
      pomodoroWithLongBreak $counter
    done
    ;;
  'pomo')
          echo "Starting Pomodoro"
          pomodoroWithShortBreak 1
          ;;
  'sb')
          echo "Short Break"
          countdown "00:05:00"
          pomodoroMesssage
          ;;
  'lb')
          echo "Long Break"
          countdown "00:15:00"
          pomodoroMesssage
          ;;
  *)
          echo
          echo "Usage: $0 { start | pomo | sb | lb }"
          echo
          exit 1
          ;;
esac


exit 0
