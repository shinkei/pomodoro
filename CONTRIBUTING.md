# Contributing?

If you would like to contribute to this project, you hae to follow some simple
steps:

- Create an issue about the funtionality or bug you would like to contribute
- Ask for contributing permisions to the owner
- Download the repository and create a branch, you should relate the branch with
  the issue.
- Create a PR of your changes

And that's it, I will more than happy to add you the the contributors list :)